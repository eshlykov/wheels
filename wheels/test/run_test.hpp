#pragma once

#include <wheels/test/test.hpp>

namespace wheels::test {

void RunTest(ITestPtr test, const GlobalOptions& options);

}  // namespace wheels::test
